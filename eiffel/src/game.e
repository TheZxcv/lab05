class
    GAME

create
    make

feature {NONE}
	rolls : ARRAY[INTEGER]
	currentRoll : INTEGER

	is_strike (i_roll: INTEGER) : BOOLEAN
		require
			rolls.valid_index (i_roll)
		do
			Result := rolls[i_roll] = 10
		end

	strike_bonus (i_roll: INTEGER) : INTEGER
		require
			rolls.valid_index (i_roll)
			rolls.valid_index (i_roll + 1)
			rolls.valid_index (i_roll + 2)
		do
			Result := rolls[i_roll + 1] + rolls[i_roll + 2]
		end

	is_spare (i_roll: INTEGER) : BOOLEAN
		require
			rolls.valid_index (i_roll)
			rolls.valid_index (i_roll + 1)
		local
			sum: INTEGER
		do
			sum := rolls[i_roll] + rolls[i_roll + 1]
			Result := sum = 10
		end

	spare_bonus (i_roll: INTEGER) : INTEGER
		require
			rolls.valid_index (i_roll + 2)
		do
			Result := rolls[i_roll + 2]
		end

	score_normal (i_roll: INTEGER) : INTEGER
		require
			rolls.valid_index (i_roll)
			rolls.valid_index (i_roll + 1)
		do
			Result := rolls[i_roll] + rolls[i_roll + 1]
		end

feature
	make
		do
			create rolls.make_filled (0, 0, 20)
			currentRoll := 0
		end

	roll (pins: INTEGER)
		require
			pins >= 0 AND pins <= 10
		do
			rolls.enter(pins, currentRoll)
			currentRoll := currentRoll + 1
			ensure
				currentRoll <= 21
		end

	score : INTEGER
		local
			i_roll : INTEGER
			total : INTEGER
		do
			total := 0
			i_roll := 0
			across 0 |..| 9 as frame
			loop
				if is_strike (i_roll) then
					total := total + 10 + strike_bonus (i_roll)
					i_roll := i_roll + 1
				elseif is_spare (i_roll) then
					total := total + 10 + spare_bonus (i_roll)
					i_roll := i_roll + 2
				else
					total := total + score_normal (i_roll)
					i_roll := i_roll + 2
				end
				Result := total
			end
			ensure
				Result >= 0 AND Result <= 300
		end

invariant
	currentRoll <= rolls.count
	across rolls as r all r.item <= 10 end
end
