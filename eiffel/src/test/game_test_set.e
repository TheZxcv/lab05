class
	GAME_TEST_SET

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE}
	on_prepare
		do
			create game.make
		end

feature {NONE} -- Test helpers
	game : GAME

	roll_strike
		require
			game /= Void
		do
			game.roll (10)
		end

	roll_spare
		require
			game /= Void
		do
			roll_many (2, 5)
		end

	roll_many (a_n : INTEGER; a_pins : INTEGER)
		require
			game /= Void
		local
			i: INTEGER
		do
			from
				i := 0
			until
				i = a_n
			loop
				i := i + 1
				game.roll (a_pins)
			end

		end

feature -- Test routines
	test_gutter_game
		do
			roll_many (20, 0)
			assert ("gutter game failed", game.score = 0)
		end

	test_all_ones
		do
			roll_many (20, 1)
			assert ("all ones failed", game.score = 20)
		end

	test_one_spare
		do
			roll_spare ()
			game.roll (3)
			roll_many (17, 0)
			assert ("one spare failed", game.score = 16)
		end

	test_one_strike
		do
			roll_strike ()
			game.roll (3)
			game.roll (4)
			roll_many (16, 0)
			assert ("one strike failed ", game.score = 24)
		end

	test_perfect_game
		do
			roll_many (12, 10)
			assert ("perfect game failed", game.score = 300)
		end

	test_last_spare
		do
			roll_many (9, 10)
			roll_spare ()
			game.roll (10)
			assert ("last spare failed", game.score = 275)
		end

end
